<?php
/**
 * Created by PhpStorm.
 * User: Dima.Barbuta
 * Date: 2/7/2018
 * Time: 12:47 PM
 */
?>


<div class="wrapper">
    <form action="/wp-admin/admin-post.php" method="post" class="form">
        <input type="hidden" name="action" value="add_post_hook">
        <div class="legend">
            <spam class="red">*</spam>
            Obligatoriu de îndeplinit
        </div>
        <div class="form_component">
            <p>Numele <span class="red">*</span></p>
            <input type="text" name="nume" class="requaerd" id="nume" placeholder="Numele">
        </div>

        <div class="form_component">
            <p>Prenumele <span class="red">*</span></p>
            <input type="text" name="prenume" class="requaerd" id="prenume" placeholder="Prenume">
        </div>

        <div class="form_component">
            <p>De unde ați aflat despre noi? <span class="red">*</span></p>
            <input type="text" name="source" class="requaerd" id="source" placeholder="De unde ați aflat despre noi?">
        </div>

        <div class="form_component">
            <p>Buletinul de identitate seria și numărul<span class="red">*</span></p>
            <input type="text" name="seria" class="requaerd" id="seria"
                   placeholder="Buletinul de identitate seria și numărul">
        </div>

        <div class="form_component">
            <p>Numarul de indentitate <span class="red">*</span></p>
            <input type="text" name="idnp" class="requaerd" id="idnp" placeholder="Numarul de indentitate">
        </div>

        <div class="form_component">
            <p>Sexul <span class="red">*</span></p>
            <div><span>M <input type="radio" name="sex" class="requaerd" id="sex_m" value="m"></span></div>
            <div><span>F <input type="radio" name="sex" class="requaerd" id="sex_f" value="f"></span></div>

        </div>

        <div class="form_component">
            <p>Data, luna și anul nașterii <span class="red">*</span></p>
            <input type="text" name="birth" class="requaerd" id="birth" placeholder="Data, luna și anul nașterii">
        </div>

        <div class="form_component">
            <p>Adresa de domiciliu<span class="red">*</span></p>
            <input type="text" name="address" class="requaerd" id="address" placeholder="Adresa de domiciliu">
        </div>

        <div class="form_component">
            <p>Telefon Mobil <span class="red">*</span></p>
            <input type="text" name="phone" class="requaerd" id="phone" placeholder="Telefon Mobil">
        </div>

        <div class="form_component">
            <p>Telefon domiciliu </p>
            <input type="text" name="phoneHome" class="requaerd" id="phoneHome" placeholder="Telefon domiciliu">
        </div>

        <div class="form_component">
            <p>Telefon serviciu </p>
            <input type="text" name="phonework" class="requaerd" id="phonework" placeholder="Telefon serviciu">
        </div>

        <div class="form_component">
            <p>Email <span class="red">*</span></p>
            <input type="text" name="email" class="requaerd" id="email" placeholder="Email" required>
        </div>

        <div class="form_component">
            <p>Studiile (Instituția de învățământ, orașul, anul) <span class="red">*</span></p>
            <input type="text" name="studies" class="requaerd" id="studies" placeholder="Studiile" required>
        </div>

        <div class="form_component">
            <p>Locul de muncă (funcția)<span class="red">*</span></p>
            <input type="text" name="work" class="requaerd" id="work" placeholder="Locul de muncă " required>
        </div>

        <div class="form_component">
            <p>Specialitatea <span class="red">*</span></p>
            <input type="text" name="specialty" class="requaerd" id="specialty" placeholder="Specialitatea">
        </div>

        <input type="submit" name="submit" value="submit">
    </form>
</div>

<?php
//
//require_once '../inc/MPDF57/mpdf.php';
//
//
//
//
//$pdf = $this->pdf->load('c');
//
//$pdf->myvariable = file_get_contents($name);
//$html = '<img src="var:myvariable" />';
//$pdf->WriteHTML($html);
//$pdf->Output();

?>


<style>

    .wrapper {

        max-width: 1200px;
        margin: 0 auto;
    }

    .form .form_component {
        font-family: OpenSans-Light;
        font-size: 14px;
        width: 50%;
        height: 65px;
        float: left;
    }

    .form .form_component {
        font-family: OpenSans-Light;
        font-size: 14px;
    }

    .red {
        color: red;
    }

    .form_component p {
        margin-bottom: 5px;
    }

    .form input {
        width: 379px;
        height: 31px;
        padding-left: 11px;
        border: 2px solid #C8C8C8;
    }
</style>