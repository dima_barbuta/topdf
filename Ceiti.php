<?php defined('ABSPATH') or die('You have no right to access this file');

/**
 * Plugin Name: Ceiti
 * Description: Patom
 * Author: eu
 * License: GPLv2 or later
 * version: 1.0
 * Text Domain: Ceiti
 */
class Ceiti_Plugin
{
    private $name = 'Inregistrari';
    private $slug = 'ceiti';

    private $pageTitle = 'Inregistrai lista';
    private $menuTitle = 'Lista Inregistrari';
    private $capability = 'manage_options';
    private $slugS = 'insregistrari_all';
    private $position = '5';
    private $icon = '';


    private $css = '/assets/css/style.css';
    private $js = '/assets/js/scripts.js';
    private $mapOptions = array(

    );

    /**
     * init plugin, add all actions here.
     */
    public function __construct()
    {
        add_action('admin_enqueue_scripts', array($this, 'registerAssets'));
        add_action('add_meta_boxes', array($this, 'registerMetaboxes'));
        add_action('save_post', array($this, 'saveMetaboxesData'));

        add_action('init', array($this, 'ceiti'));
        register_activation_hook(__FILE__, array($this, 'registerNewPage'));
        add_action('admin_post_add_post_hook', array($this, 'registerNewPost'));
        add_action('admin_menu', array($this,'registerMapItemMenu') );


    }




    public function registerMapItemMenu()
    {
        add_menu_page( $this->pageTitle, $this->menuTitle, $this->capability, $this->slugS, array($this,'mapView'), $this->icon, $this->position );
        //register option
        foreach ($this->mapOptions as $mapOption ) {
            register_setting( 'map-group', $mapOption );
        }
    }


    public function mapView()
    {
        include 'view/inregistrariAll.php';
    }


    public function ceiti()
    {

        $this->registerCustomPost();
    }

    /**
     * Static method for unistall plugin.
     */
    public static function uninstallPlugin()
    {
        if (self::userAccess() && self::adminReffer()) {
            // TODO: Delete all posts type 'spot'
        }
    }

    /**
     * Method for check current user access lvl.
     *
     * @return bool.
     */
    public static function userAccess()
    {
        return (current_user_can('activate_plugins')) ? true : false;
    }

    /**
     * Method for check request reffer.
     *
     * @return bool return bool or break script execution.
     */
    public static function adminReffer()
    {
        $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
        check_admin_referer("bulk-plugins");

        return true;
    }

    /**
     * Method for register custom post.
     *
     * @param WP_Post $post Current post object.
     */


    public function registerNewPage($post_id)
    {

        global $wpdb;
        ob_start();
        include "view/form.php";
        $content = ob_get_clean();


        $PageGuid = site_url() . "/ceiti-page";
        $my_post = array('post_title' => 'Form',
            'post_type' => 'page',
            'post_name' => 'ceiti-page',
            'post_content' => $content,
            'post_status' => 'publish',
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_author' => 1,
            'menu_order' => 0,
            'guid' => $PageGuid
        );

        $PageID = wp_insert_post($my_post, FALSE);

        $query = "UPDATE {$wpdb->prefix}posts SET post_type='page' WHERE id='" . $PageID . "' LIMIT 1";

        $wpdb->query($query);


//        $this->createTable();

    }


    public function registerNewPost()
    {

        $newPost = wp_insert_post(
            array(
                'post_title' => $_POST['nume'] . ' ' . $_POST['prenume'],
                'post_type' => $this->slug,
                'post_content' => 'demo texxt',

            )
        );


//        $this->nume($_POST);
    }


    public function registerCustomPost()
    {

        $labels = array(
            'name' => $this->name,
            'singular_name' => $this->name,
            'add_new' => _x('Add New', $this->name, $this->name),
            'add_new_item' => _x('Add New Team Member', $this->name),
        );

        $arguments = array(
            'label' => $this->name,
            'labels' => $labels,
            'description' => '',
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-admin-users',
            'supports' => array('title', 'editor'),
            // 'taxonomies'          => array('category'),
            // 'rewrite'             => array( 'slug' => 'book' ),

        );

        register_post_type($this->slug, $arguments);

    }


    public function registerAssets()
    {
        wp_enqueue_media();
        wp_enqueue_script('media-upload');
        wp_enqueue_style($this->slug . '-css', plugin_dir_url(__FILE__) . $this->css);
        wp_enqueue_script($this->slug . '-js', plugin_dir_url(__FILE__) . $this->js);
    }

    public function registerMetaboxes()
    {
        add_meta_box('downloand', _x('Downloand', $this->name), array($this, 'downloand'), $this->slug);
        add_meta_box('nume', _x('Nume', $this->name), array($this, 'nume'), $this->slug);
        add_meta_box('prenume', _x('Prenume', $this->name), array($this, 'prenume'), $this->slug);
        add_meta_box('source', _x('Source', $this->name), array($this, 'source'), $this->slug);
        add_meta_box('seria', _x('Seria', $this->name), array($this, 'seria'), $this->slug);
        add_meta_box('idnp', _x('Idnp', $this->name), array($this, 'idnp'), $this->slug);
        add_meta_box('sex', _x('Sex', $this->name), array($this, 'sex'), $this->slug);
        add_meta_box('birth', _x('Birth', $this->name), array($this, 'birth'), $this->slug);
        add_meta_box('address', _x('Address', $this->name), array($this, 'address'), $this->slug);
        add_meta_box('phone', _x('Phone', $this->name), array($this, 'phone'), $this->slug);
        add_meta_box('phoneHome', _x('PhoneHome', $this->name), array($this, 'phoneHome'), $this->slug);
        add_meta_box('phonework', _x('Phonework', $this->name), array($this, 'phonework'), $this->slug);
        add_meta_box('email', _x('Email', $this->name), array($this, 'email'), $this->slug);
        add_meta_box('studies', _x('Studies', $this->name), array($this, 'studies'), $this->slug);
        add_meta_box('work', _x('Work', $this->name), array($this, 'work'), $this->slug);
        add_meta_box('specialty', _x('Specialty', $this->name), array($this, 'specialty'), $this->slug);


    }


    public function saveMetaboxesData($post_id)
    {
        update_post_meta($post_id, 'nume', $_POST['nume']);
        update_post_meta($post_id, 'prenume', $_POST['prenume']);
        update_post_meta($post_id, 'source', $_POST['source']);
        update_post_meta($post_id, 'seria', $_POST['seria']);
        update_post_meta($post_id, 'idnp', $_POST['idnp']);
        update_post_meta($post_id, 'sex', $_POST['sex']);
        update_post_meta($post_id, 'birth', $_POST['birth']);
        update_post_meta($post_id, 'address', $_POST['address']);
        update_post_meta($post_id, 'phone', $_POST['phone']);
        update_post_meta($post_id, 'phoneHome', $_POST['phoneHome']);
        update_post_meta($post_id, 'phonework', $_POST['phonework']);
        update_post_meta($post_id, 'email', $_POST['email']);
        update_post_meta($post_id, 'studies', $_POST['studies']);
        update_post_meta($post_id, 'work', $_POST['work']);
        update_post_meta($post_id, 'specialty', $_POST['specialty']);
    }


    public function downloand($post)
    {
        ?>
        <div id="downloand">
            <div>

                <button id="downloadPdf" type="button">Down</button>

            </div>
        </div>
        <?php
    }


    public function nume($post)
    {
        $nume = get_post_meta($post->ID, 'nume', true);
        ?>
        <div id="nume">
            <div>
                <input disabled name="nume" id="nume" class="" value="<?php echo $nume; ?>">
            </div>
        </div>
        <?php
    }


    public function prenume($post)
    {
        $prenume = get_post_meta($post->ID, 'prenume', true);
        ?>
        <div id="prenume">
            <div>
                <input disabled name="prenume" id="prenume" class="" value="<?php echo $prenume; ?>">
            </div>
        </div>
        <?php
    }


    public function source($post)
    {
        $source = get_post_meta($post->ID, 'source', true);
        ?>
        <div id="source">
            <div>
                <input disabled name="source" id="source" class="" value="<?php echo $source; ?>">
            </div>
        </div>
        <?php
    }


    public function seria($post)
    {
        $seria = get_post_meta($post->ID, 'seria', true);
        ?>
        <div id="seria">
            <div>
                <input disabled name="seria" id="seria" class="" value="<?php echo $seria; ?>">
            </div>
        </div>
        <?php
    }


    public function idnp($post)
    {
        $idnp = get_post_meta($post->ID, 'idnp', true);
        ?>
        <div id="idnp">
            <div>
                <input disabled name="idnp" id="idnp" class="" value="<?php echo $idnp; ?>">
            </div>
        </div>
        <?php
    }


    public function sex($post)
    {
        $sex = get_post_meta($post->ID, 'sex', true);
        ?>
        <div id="sex">
            <div>
                <input disabled name="sex" id="sex" class="" value="<?php echo $sex; ?>">
            </div>
        </div>
        <?php
    }

    public function birth($post)
    {
        $birth = get_post_meta($post->ID, 'birth', true);
        ?>
        <div id="birth">
            <div>
                <input disabled name="birth" id="birth" class="" value="<?php echo $birth; ?>">
            </div>
        </div>
        <?php
    }


    public function address($post)
    {
        $address = get_post_meta($post->ID, 'address', true);
        ?>
        <div id="address">
            <div>
                <input disabled name="address" id="address" class="" value="<?php echo $address; ?>">
            </div>
        </div>
        <?php
    }


    public function phone($post)
    {
        $phone = get_post_meta($post->ID, 'phone', true);
        ?>
        <div id="phone">
            <div>
                <input disabled name="phone" id="phone" class="" value="<?php echo $phone; ?>">
            </div>
        </div>
        <?php
    }


    public function phoneHome($post)
    {
        $phoneHome = get_post_meta($post->ID, 'phoneHome', true);
        ?>
        <div id="phoneHome">
            <div>
                <input disabled name="phoneHome" id="phoneHome" class="" value="<?php echo $phoneHome; ?>">
            </div>
        </div>
        <?php
    }


    public function phonework($post)
    {
        $phonework = get_post_meta($post->ID, 'phonework', true);
        ?>
        <div id="phonework">
            <div>
                <input disabled name="phonework" id="phonework" class="" value="<?php echo $phonework; ?>">
            </div>
        </div>
        <?php
    }


    public function email($post)
    {
        $email = get_post_meta($post->ID, 'email', true);
        ?>
        <div id="email">
            <div>
                <input disabled name="email" id="email" class="" value="<?php echo $email; ?>">
            </div>
        </div>
        <?php
    }


    public function studies($post)
    {
        $studies = get_post_meta($post->ID, 'studies', true);
        ?>
        <div id="studies">
            <div>
                <input disabled name="studies" id="studies" class="" value="<?php echo $studies; ?>">
            </div>
        </div>
        <?php
    }


    public function work($post)
    {
        $work = get_post_meta($post->ID, 'work', true);
        ?>
        <div id="work">
            <div>
                <input disabled name="work" id="work" class="" value="<?php echo $work; ?>">
            </div>
        </div>
        <?php
    }


    public function specialty($post)
    {
        $specialty = get_post_meta($post->ID, 'specialty', true);
        ?>
        <div id="specialty">
            <div>
                <input disabled name="specialty" id="specialty" class="" value="<?php echo $specialty; ?>">
            </div>
        </div>
        <?php
    }


    public function createTable()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . "inregistrare_cursuri";

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS  $table_name (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
               name varchar(255) DEFAULT '' NOT NULL,
              surname varchar(255) DEFAULT '' NOT NULL,
               source text DEFAULT '' NOT NULL,
               seria text DEFAULT  '' NOT NULL,
                idnp int DEFAULT NULL ,
              sex varchar(1) DEFAULT '' NOT NULL,
              birth date DEFAULT '0000-00-00' NOT NULL,
               address text DEFAULT '' NOT NULL,
               phone text DEFAULT '' NOT NULL,
               phoneHome text DEFAULT '',
               phoneWork text DEFAULT '',
               email text DEFAULT '' NOT NULL,
               studies text DEFAULT '' NOT NULL,
               work text DEFAULT '' NOT NULL,
               specialty text DEFAULT '' NOT NULL,
               period text DEFAULT '' NOT NULL,
              PRIMARY KEY  (id)
              )$charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

new Ceiti_Plugin();